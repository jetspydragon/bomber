var x1 = floor(random_range(1, 45));
var y1 = 43;

target = instance_create(x1, y1, objTarget);
target.image_blend = c_gray;

for (var n = x1+1; n < 80; n++) {
    wall = instance_create(n, y1, objWall);
    wall.image_blend = c_ltgray;
    wall.base = false;
}