/// ea_string_lpad(cadena, longitud, caracter)
var cadena = argument0;
var longitud = argument1;
var caracter = argument2;

var cad = cadena;
var len = string_length(cadena);

if (len > longitud) {
    cad = string_copy(cadena, 0, longiud);
}
if (len < longitud) {
    cad = string_repeat(caracter, longitud-len) + cadena;
}

return cad;
