switch (argument[0]) {
    
case "Create":
    score = 0;
    // Dibujo la base
    for (var n = 0; n < 80; n++) {
        var wall = instance_create(n, 42, objWall);
        wall.image_blend = c_black;
    }
    // Dibujo el objetivo
    scrDrawTarget();
    
    self.bomber = instance_create(80, 0, objBomber);
    self.canShoot = true;
    self.intents = 1;
    
    break;

case "DrawGUI":
    draw_set_font(fntFont);
    draw_set_colour(c_black);
    draw_text(1*VIEW_TO_PORT, 44*VIEW_TO_PORT, "Score:");
    draw_set_colour(c_green);
    draw_text(35*VIEW_TO_PORT, 44*VIEW_TO_PORT, score);
    break;
    
case "K_Escape":
    room_goto(rmMenu);    
    break;

case "Global_Left_Button":
    if (self.canShoot) {
        instance_create(self.bomber.x+6, self.bomber.y+8, objBomb);
        self.canShoot = false;
    }    
    break;    

case "Step":
    if (self.bomber.x < -self.bomber.sprite_width) {
        self.bomber.x = view_wview[0];
        self.canShoot = true;
        self.intents++;
    }
    break;    
    
}
